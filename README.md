# e3-ioc-isrc-timtest

Test IOC with an EVR to debug the ISrc RF probes in the lab

## Cloning

Clone this IOC with `git clone https://gitlab.esss.lu.se/iocs/lab/e3-ioc-isrc-timtest.git`.

## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of the modules required in `st.cmd` are installed for the EPICS base and `require` version that you are using.

If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.

## History

Originally this IOC was manually written by Jerzy locally in isrc-mtca-ioc01.tn.esss.lu.se under /nfs/ts/nbs/Ctrl-EVR-101.

